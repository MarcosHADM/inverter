<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Logo do Projeto">
  </a>
</p>


<h3 align="center">Inversor com ESP32 e WiFi</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-ativo-sucesso?style=for-the-badge)]()
[![GitHub Issues](https://img.shields.io/github/issues/MarcosHADM/inverter?style=for-the-badge)](https://github.com/MarcosHADM/inverter/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/MarcosHADM/inverter?style=for-the-badge)](https://github.com/MarcosHADM/inverter/pulls)
[![Licença](https://img.shields.io/github/license/MarcosHADM/inverter?style=for-the-badge)](/LICENSE)

</div>

---

<p align="center"> Este projeto implementa um inversor de tensão utilizando o ESP32 como controlador. Além disso, o controlador é capaz de se conectar a uma rede WiFi e receber comandos via rede para controlar a saída do inversor.
    <br> 
</p>

## 📝 Sumário

- [🧐 Sobre](#sobre)
- [🏁 Como Começar](#como-comecar)
    - [📋 Pré-requisitos](#pre-requisitos)
    - [🏗️ Compilando e Carregando o Código](#compilando)
- [🎈 Como Usar](#uso)
- [⛏️ Construído Utilizando](#construído-utilizando)
- [📝 Licença](#licença)
- [✍️ Autores](#autores)
- [🎉 Agradecimentos](#agradecimentos)


## 🧐 Sobre <a name = "sobre"></a>
O inversor é capaz de converter uma tensão contínua em uma tensão alternada, possibilitando o uso de dispositivos AC em locais sem acesso a uma rede elétrica. O ESP32 é utilizado como controlador do inversor e também se conecta a uma rede WiFi para permitir o controle do inversor por meio de comandos enviados via rede.


## 🏁 Como Começar <a name = "como-comecar"></a>
Estas instruções fornecerão uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste.


### 📋 Pré-requisitos <a name = "pre-requisitos"></a>
O que você precisa para compilar e carregar o código no ESP32:

- [PlatformIO](https://platformio.org)
- [ESP32 toolchain]()
- [ESPAsyncWebServer](https://registry.platformio.org/libraries/ottowinter/ESPAsyncWebServer-esphome)


### 🏗️ Compilando e Instalando <a name = "compilando"></a>
Descreva aqui os passos que o usuário deve seguir para instalar e rodar o seu projeto.


## 🎈 Como usar <a name="uso"></a>
Aqui você pode adicionar notas sobre como usar o seu projeto.


## ⛏️ Construído Utilizando <a name = "construído-utilizando"></a>
- [PlatformIO]() - 
- [ESP32](https://www.espressif.com/en/products/socs/esp32/overview) - Controlador de hardware


## 📝 Licença <a name = "licença"></a>
Este projeto está licenciado sob a Licença MIT - veja o arquivo [LICENSE](/LICENSE) para mais detalhes.


## ✍️ Autores <a name = "autores"></a>
- [Marcos Helbert](https://github.com/MarcosHADM) - Ideia e trabalho inicial


## 🎉 Agradecimentos <a name = "agradecimentos"></a>
- [PlatformIO](https://platformio.org)