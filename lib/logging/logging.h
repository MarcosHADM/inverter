#ifndef LOGGING_H
#define LOGGING_H

#include <Arduino.h>

#define BUFFER_SIZE 256

#define TEXT_BOLD "\033[1m"         // negrito
#define TEXT_UNDERLINE "\033[4m"    // sublinhado
#define TEXT_BLINK "\033[5m"        // piscar
#define TEXT_REVERSE "\033[7m"      // inverter
#define TEXT_RESET "\033[0m"        // restaurar

#define COLOR_CYAN "\033[0;36m"     // ciano
#define COLOR_GREEN "\033[0;32m"    // verde
#define COLOR_YELLOW "\033[0;33m"   // amarelo
#define COLOR_RED "\033[0;31m"      // vermelho
#define COLOR_RESET "\033[0m"       // restaurar cor

class Logger {
    public:
        enum LogLevel {
            Debug,
            Info,
            Warn,
            Error,
            Critical
        };

        Logger(LogLevel log_level = Debug) : log_level_(log_level) {
            formatter_.setFormat("{levelname} - {message}");
        };

        void setLevel(LogLevel log_level) {
            log_level_ = log_level;
        }

        void setFormatter(const String& format) {
            formatter_.setFormat(format);
        }

        void debug(const char* text, ...) {
            if (log_level_ > Debug) return;
            char buffer[BUFFER_SIZE];
            va_list args;
            va_start(args, text);
            vsnprintf(buffer, BUFFER_SIZE, text, args);
            va_end(args);

            log(COLOR_CYAN, "DEBUG", buffer);
        };

        void info(const char* text, ...) {
            if (log_level_ > Info) return;
            char buffer[BUFFER_SIZE];
            va_list args;
            va_start(args, text);
            vsnprintf(buffer, BUFFER_SIZE, text, args);
            va_end(args);

            log(COLOR_GREEN, "INFO", buffer);
        };

        void warning(const char* text, ...) {
            if (log_level_ > Warn) return;
            char buffer[BUFFER_SIZE];
            va_list args;
            va_start(args, text);
            vsnprintf(buffer, BUFFER_SIZE, text, args);
            va_end(args);

            log(COLOR_YELLOW, "WARNING", buffer);
        };

        void error(const char* text, ...) {
            if (log_level_ > Error) return;
            char buffer[BUFFER_SIZE];
            va_list args;
            va_start(args, text);
            vsnprintf(buffer, BUFFER_SIZE, text, args);
            va_end(args);

            log(COLOR_RED, "ERROR", buffer);
        };

        void critical(const char* text, ...) {
            if (log_level_ > Critical) return;
            char buffer[BUFFER_SIZE];
            va_list args;
            va_start(args, text);
            vsnprintf(buffer, BUFFER_SIZE, text, args);
            va_end(args);
            String level = "\033[1mCRITICAL\033[0m";

            log(COLOR_RED, (level + COLOR_RED).c_str(), buffer);
        };

    private:
        void log(const char* color, const char* prefix, char* buffer) {
            String formatted_message = formatter_.format({
                {"time", getTime()},
                {"level", prefix},
                {"file", __FILE__},
                {"function", __FUNCTION__},
                {"lineno", String(__LINE__)},
                {"message", buffer}
            });

            size_t color_length = strlen(color);
            size_t message_length = formatted_message.length();
            size_t reset_color_length = strlen(COLOR_RESET);
            size_t total_length = color_length + message_length + reset_color_length + 2;
            char output_buffer[total_length];

            memcpy(output_buffer, color, color_length);
            memcpy(output_buffer + color_length, formatted_message.c_str(), message_length);
            memcpy(output_buffer + color_length + message_length, COLOR_RESET, reset_color_length);
            memcpy(output_buffer + color_length + message_length + reset_color_length, "\r\n", 2);
            Serial.write(output_buffer, total_length);
        }

        String getTime() {
            //TODO: fazer a função retornar a hora atual
            String time = "Mon, 14 Dec 2022 00:21:42";
            return time;
        }

        class Formatter {
            public:
                void setFormat(const String& format) {
                    format_ = format;
                }

                String format(const std::initializer_list<std::pair<const char*, String>>& values) {
                    String formatted_message = format_;
                    for (auto const & value : values) {
                        formatted_message.replace("{" + String(value.first) + "}", value.second);
                    }
                    return formatted_message;
                }
            
            private:
                String format_;
        } formatter_;

        LogLevel log_level_;
};

#endif