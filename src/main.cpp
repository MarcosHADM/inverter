//#include <WiFiManager.h> 
#include <Arduino.h>
#include <WiFi.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>
#include <SPIFFS.h>

#include "logging.h"
Logger logger;

AsyncWebServer server(80);

const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
const char* PARAM_INPUT_3 = "ip";
const char* PARAM_INPUT_4 = "gateway";

String ssid;
String pass;
String ip;
String gateway;

const char* ssidPath = "/WiFiData/ssid.txt";
const char* passPath = "/WiFiData/pass.txt";
const char* ipPath = "/WiFiData/ip.txt";
const char* gatewayPath = "/WiFiData/gateway.txt";

IPAddress localIP;
IPAddress localGateway;
IPAddress subnet(255, 255, 0, 0);

unsigned long previousMillis = 0;
const long interval = 10000;

const int ledPin = 2;
String ledState;

/*
IPAddress apIP(8, 8, 4, 4);
DNSServer dnsServer;
AsyncWebServer server(80);
*/

String getEncryptionTypeName(int encryptionType) {
    String encryption = "";
    switch (encryptionType) {
        case 0: 
            encryption = "OPEN";
            break;
        case 1: 
            encryption = "WEP";
            break;
        case 2: 
            encryption = "WPA_PSK";
            break;
        case 3: 
            encryption = "WPA2_PSK";
            break;
        case 4: 
            encryption = "WPA_WPA2_PSK";
            break;
        case 5: 
            encryption = "WPA2_ENTERPRISE";
            break;
        case 6: 
            encryption = "MAX";
            break;
        default:
            encryption = "Unknown";
            break;
    }

    return encryption;
}

void initSPIFFS() {
    if (!SPIFFS.begin(true)) {
        logger.error("An error has occurred while mounting SPIFFS");
    }
    logger.info("SPIFFS mounted successfully");
}

String readFile(fs::FS &fs, const char * path) {
    logger.debug("Reading file: %s", path);

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        logger.error("Failed to open file (%s) for reading", path);
        return String();
    }

    String fileContent;
    while (file.available()) {
        fileContent = file.readStringUntil('\n');
        break;
    }

    return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
    logger.debug("Writing file: %s", path);

    File file = fs.open(path, FILE_WRITE);

    if (!file) {
        logger.error("Failed to open file(%s) for writing", path);
        return;
    }

    if (file.print(message)) {
        logger.debug("File(%s) written", path);
    } else {
        logger.error("File(%s) write failed", path);
    }
}

bool initWiFi() {
    if (ssid == "" or ssid == NULL) {
        logger.warning("Undefined SSID.");
        return false;
    } else if (ip == "" or ip == NULL) {
        logger.warning("Undefined IP address.");
        return false;
    }

    WiFi.mode(WIFI_STA);
    localIP.fromString(ip.c_str());
    localGateway.fromString(gateway.c_str());

    if (!WiFi.config(localIP, localGateway, subnet)) {
        logger.warning("STA Failed to configure");
        return false;
    }

    WiFi.begin(ssid.c_str(), pass.c_str());
    logger.info("Connecting to WiFi...");

    unsigned long currentMillis = millis();
    previousMillis = currentMillis;

    while(WiFi.status() != WL_CONNECTED) {
        currentMillis = millis();
        if (currentMillis - previousMillis >= interval) {
            logger.warning("Failed to connect.");
            return false;
        }
    }
    
    //WiFi.scanNetworks(true);
    logger.info("------------------------------------\n"
                "AP IP address: %s \n"
                "------------------------------------"
                , WiFi.localIP().toString().c_str());
    return true;
}

String processor(const String& var) {
    if(var == "STATE") {
        if (digitalRead(ledPin)) {
            ledState = "ON";
        } else {
            ledState = "OFF";
        }

        return ledState;
    }

    return String();
}


// Handle AP mode
void ap_root(AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/wifimanager/index.html", "text/html");
};

void ap_wifi_scan(AsyncWebServerRequest *request) {
    String json = "[";
    int n = WiFi.scanComplete();
    logger.debug("Scan complete: %d", n);
    if(n == -2) {
        WiFi.scanNetworks(true);
        logger.debug("No saved scan results, starting new scan");
    } else if(n) {
        for (int i = 0; i < n; i++) {
            Serial.print("Iniciando Json para rede ");
            Serial.println(i);
            if(i) json += ",";
            json += "{";
            json += "\"rssi\":" + String(WiFi.RSSI(i));
            json += ",\"ssid\":\"" + WiFi.SSID(i) + "\"";
            json += ",\"bssid\":\"" + WiFi.BSSIDstr(i) + "\"";
            json += ",\"channel\":" + String(WiFi.channel(i));
            json += ",\"secure\":" + String(WiFi.encryptionType(i));
            json += "}";
        }
        WiFi.scanDelete();
        if(WiFi.scanComplete() == -2) {
            WiFi.scanNetworks(true);
        }
    }

    json += "]";
    request->send(200, "text/json", json);
};

/*
void handle_scan(AsyncWebServerRequest *request) {
    String json = "[";
    int n = WiFi.scanComplete();
    Serial.println(n);
    if(n == -2) {
        WiFi.scanNetworks(true);
        Serial.println("Não há redes scaneadas salvas, iniciando nova busca");
    } else if(n) {
        for (int i = 0; i < n; i++) {
            Serial.print("Iniciando Json para rede ");
            Serial.println(i);
            if(i) json += ",";
            json += "{";
            json += "\"rssi\":" + String(WiFi.RSSI(i));
            json += ",\"ssid\":\"" + WiFi.SSID(i) + "\"";
            json += ",\"bssid\":\"" + WiFi.BSSIDstr(i) + "\"";
            json += ",\"channel\":" + String(WiFi.channel(i));
            json += ",\"secure\":" + String(WiFi.encryptionType(i));
            json += "}";
        }
        WiFi.scanDelete();
        if(WiFi.scanComplete() == -2) {
            WiFi.scanNetworks(true);
        }
    }

    json += "]";
    request->send(200, "text/json", json);
    json = String();
}
*/

void ap_wifi_connect(AsyncWebServerRequest *request) {
    if (!request->hasParam("ssid") || !request->hasParam("criptografia") || !request->hasParam("rssi")) {
        logger.debug(""
                    "-----------------------------\n"
                    "Paramentro SSID: %s\n"
                    "Paramentro Criptografia: %s\n"
                    "Paramentro RSSI: %s\n"
                    "-----------------------------\n"
                    , request->getParam("ssid")->value().c_str(), request->getParam("criptografia")->value().c_str(), request->getParam("rssi")->value().c_str()
        );
        request->redirect("/");
        return;
    }

    String ssid = request->getParam("ssid")->value();
    String criptografia = request->getParam("criptografia")->value();
    String rssi = request->getParam("rssi")->value();

    if (request->hasParam("password")) {
        String password = request->getParam("password")->value();
        if (password == "" or password == NULL) {
            logger.warning("Undefined password.");
            return;
        }
        logger.debug(""
                    "ssid: %s\n"
                    "Password: %s"
                    "", ssid, password);
        WiFi.begin(ssid.c_str(), password.c_str());
        logger.info("Connecting to WiFi...");
        return;
    }

    logger.debug(""
                "-----------------------------\n"
                "SSID: %s\n"
                "Criptografia: %s\n"
                "RSSI: %s\n"
                "-----------------------------\n"
                , ssid.c_str(), criptografia.c_str(), rssi.c_str()
    );

    request->send(SPIFFS, "/wifimanager/connect.html", "text/html");
};

void ap_notFound(AsyncWebServerRequest *request) {
    request->redirect("/");
};



void setup() {
    Serial.begin(250000);
    WiFi.scanNetworks();
    //logger.setFormatter("[{time}] {level} [{file} {function} {lineno}] {message}");
    logger.setFormatter("[{level}] - {message}");
    logger.setLevel(Logger::Debug);
    
    initSPIFFS();

    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, LOW);

    ssid = readFile(SPIFFS, ssidPath);
    pass = readFile(SPIFFS, passPath);
    ip = readFile(SPIFFS, ipPath);
    gateway = readFile(SPIFFS, gatewayPath);

    logger.info(""
    "------------------------------------\n"
    "SSID: %s \n"
    "PASS: %s \n"
    "IP: %s \n"
    "GATEWAY: %s \n"
    "------------------------------------"
    , ssid.c_str(), pass.c_str(), ip.c_str(), gateway.c_str()
    );

    if(initWiFi()) {
        /*server.on("/", HTTP_GET, handle_root);
        server.serveStatic("/", SPIFFS, "/");

        logger.info("Starting server...");

        server.begin();*/
    } else {
        logger.info("Setting AP(Access Point) mode...");
        WiFi.softAP("ESP32-CaptivePortal", NULL);

        IPAddress IP = WiFi.softAPIP();
        logger.info("AP IP address: %s", IP.toString().c_str());
    
        server.on("/", HTTP_GET, ap_root);
        server.serveStatic("/", SPIFFS, "/");

        server.on("/scan", HTTP_GET, ap_wifi_scan);
        server.on("/connect", HTTP_GET, ap_wifi_connect);

        server.onNotFound(ap_notFound);

        server.begin();
    }
    /*
    dnsServer.start(53, "*", apIP);

    server.on("/", HTTP_GET, handle_root);
    server.on("/scan", HTTP_GET, handle_scan);
    server.on("/connect", HTTP_GET, handle_connect);
    server.onNotFound(handleNotFound);
    server.begin();

    Serial.println("Server started");*/
}

int lastStatus = 0;

void loop() {
    //dnsServer.processNextRequest();
    if (WiFi.status() != lastStatus) {
        logger.info("WiFi status: %d", WiFi.status());
        lastStatus = WiFi.status();
    }
}

